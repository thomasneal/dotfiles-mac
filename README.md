# README #

*Install [Homebrew](http://brew.sh/index.html)

*Install vcprompt with Homebrew:
`brew install vcprompt`

*Move the bashrc file to ~/username/.bashrc

*Also in ~/username/ create a .bash_profile and edit it to contain:
`if [ -f ~/.bashrc ]; then . ~/.bashrc; fi`